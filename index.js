const express = require('express');
const app = express();
const http = require('http');
const { emit } = require('process');
const server = http.createServer(app);
const { Server, Socket } = require("socket.io");
const io = new Server(server);

/*
io.on("connection", (socket) => {
    console.log("User has connected")
}) 
*/
app.use(express.json());

const stocks = [
    {
        "stockName" : 'ABC Traders',
        "price" : 44.51, 
    },
    {
        "stockName": "Trendy Fintech",
        "price": 23.99,
    },
    {
        "stockName": "SuperCorp",
        "price" : 126.91,
    }
  ];

  //Function that checks if stockname exists in stocks, and also changes the relevant price in Stocks if it does
  function checkForDataInStocks(inputKey){
    for (i = 0; i <= 2; i++){
            
        if (stocks[i].stockName == inputKey.name)
        {
            stocks[i].price = inputKey.id;
            return true;
        }
  }
  return false;
}

//Accessing current information on the stock prices
  app.get('/current', (req, res) => {
      res.json(stocks);
      console.log("Accessing current stock data");
  });

  //HTML page access
  app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
    console.log("Accessing page");
});

//The logic that checks if the patch call is correct, and if so emits a signal to all connected clients
app.patch('/update', (req, res) => {



        if (checkForDataInStocks(req.body)){
        console.log(req.body.name, "was updated successfully!!!");
       
        res.json(req.body);
        io.emit("stockUpdated", req.body);
        
        }
        else
        {
            console.log("Stock not found in range");
            res.status(404).json("Stock not found in range");
            
        }

        
    }
);
        



server.listen(3000, () => {
    console.log('Iron Bank Stock Exchange initialized');
});